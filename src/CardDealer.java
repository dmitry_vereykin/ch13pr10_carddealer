/**
 * Created by Dmitry on 7/27/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class CardDealer extends JFrame {
    private String[] back;
    private int index;
    private Random random;
    private ArrayList<File> files;
    private int numberOfCards;
    private File selectedDirectory;
    private BackFacePanel backFace;
    private TopBanner topBanner;
    private JPanel leftImagePanel;
    private JPanel rightImagePanel;
    private JLabel leftImageLabel;
    private JLabel rightImageLabel;
    private JPanel buttonPanel;
    private JButton button;
    private JFileChooser fileChooser;
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem exitItem;
    private JMenuItem buttonItem;

    public CardDealer() {
        this.setTitle("Card Dealer");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        buildMenuBar();
        buildLeftImagePanel();
        buildRightImagePanel();
        buildButtonPanel();
        this.setJMenuBar(menuBar);

        topBanner = new TopBanner();
        backFace = new BackFacePanel();

        this.add(topBanner, BorderLayout.NORTH);
        this.add(backFace, BorderLayout.WEST);
        this.add(leftImagePanel, BorderLayout.CENTER);
        this.add(rightImagePanel, BorderLayout.EAST);
        this.add(buttonPanel, BorderLayout.SOUTH);

        fileChooser = new JFileChooser(".");
        this.pack();
        this.setVisible(true);
        setLocationRelativeTo(null);
    }

    private void buildMenuBar() {
        menuBar = new JMenuBar();
        buildFileMenu();
        menuBar.add(fileMenu);
    }

    private void buildFileMenu() {
        fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);

        buttonItem = new JMenuItem("Open");
        buttonItem.setMnemonic(KeyEvent.VK_O);
        buttonItem.addActionListener(new ButtonListener());

        exitItem = new JMenuItem("Exit");
        exitItem.setMnemonic(KeyEvent.VK_X);
        exitItem.addActionListener(new ExitListener());

        fileMenu.add(buttonItem);
        fileMenu.add(exitItem);
    }

    private void buildLeftImagePanel() {
        leftImagePanel = new JPanel();
        leftImageLabel = new JLabel();
        leftImagePanel.add(leftImageLabel);
    }

    private void buildRightImagePanel() {
        rightImagePanel = new JPanel();
        rightImageLabel = new JLabel();
        rightImagePanel.add(rightImageLabel);
    }

    private void buildButtonPanel() {
        buttonPanel = new JPanel();

        button = new JButton("Deal a Card");
        button.setMnemonic(KeyEvent.VK_G);
        button.setToolTipText("Do not forget to choose \"Cards\" folder first");
        button.addActionListener(new DealButtonListener());
        buttonPanel.add(button);
    }

    private class DealButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            File selectedFile;
            ImageIcon image;
            String filename;

            try {
                if (!selectedDirectory.equals(null)) {
                    index = random.nextInt(files.size());
                    selectedFile = files.get(index);
                    filename = selectedFile.getPath();
                    image = new ImageIcon(filename);
                    rightImageLabel.setIcon(image);
                    rightImageLabel.setText(null);
                    pack();
                    files.remove(index);
                }
            } catch (Exception exc) {
                JOptionPane.showMessageDialog(null, "No more cards. Choose \"Cards\" folder");
            }
        }
    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            random = new Random();
            files = new ArrayList<File>();
            index = 0;
            int fileChooserStatus;
            back = new String[2];

            fileChooserStatus = fileChooser.showOpenDialog(CardDealer.this);

            if (fileChooserStatus == JFileChooser.APPROVE_OPTION) {
                selectedDirectory = fileChooser.getCurrentDirectory();
                File f = new File(String.valueOf(selectedDirectory));
                files = new ArrayList<File>(Arrays.asList(f.listFiles()));
                numberOfCards = files.size();
                pack();

                for (int n = 0; n < files.size(); n++) {
                    if ((files.get(n).getName()).equals("Thumbs.db")) {  //Was too lazy to figure out how filter works
                        files.remove(n);
                    }
                }

                for (int i = 0; i < files.size(); i++) {
                    if ((files.get(i).getName()).equals("Backface_Red.jpg")) {
                        back[0] = files.get(i).getPath();
                        files.remove(i);
                    }
                }

                for (int n = 0; n < files.size(); n++) {
                    if ((files.get(n).getName()).equals("Backface_Blue.jpg")) {
                        back[1] = files.get(n).getPath();
                        files.remove(n);
                    }
                }
            }
        }
    }

    private class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }

    class BackFacePanel extends JPanel {
        private JButton red;
        private JButton blue;

        public BackFacePanel() {
            setLayout(new GridLayout(7, 1));

            red = new JButton("Red Back");
            blue = new JButton("Blue Back");

            red.addActionListener(new LeftRadioListener());
            blue.addActionListener(new RightRadioListener());

            setBorder(BorderFactory.createTitledBorder("Backface: "));

            add(new JLabel("    "));
            add(new JLabel("    "));
            add(red);
            add(new JLabel("    "));
            add(blue);
            add(new JLabel("    "));
            add(new JLabel("    "));
        }

        private class LeftRadioListener implements ActionListener {
            public void actionPerformed(ActionEvent e){
                try {
                    if (!selectedDirectory.equals(null)) {
                        leftImageLabel.setIcon(new ImageIcon(back[0]));
                    }
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(null, "No cards.");
                }

                pack();
            }
        }

        private class RightRadioListener implements ActionListener {
            public void actionPerformed(ActionEvent e){
                try {
                    if (!selectedDirectory.equals(null)) {
                        leftImageLabel.setIcon(new ImageIcon(back[1]));
                    }
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(null, "No cards.");
                }

                pack();
            }
        }
    }

    class TopBanner extends JPanel {
        private JLabel topBanner;

        public TopBanner() {
            topBanner = new JLabel("Open file menu and select any file from \"Cards\" folder");
            add(topBanner);
        }
    }

    public static void main(String[] args) {
        new CardDealer();
    }
}
